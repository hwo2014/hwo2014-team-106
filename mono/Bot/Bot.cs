﻿using System;
using System.IO;
using Newtonsoft.Json;
using TeamBarrelRollers.Net;
using TeamBarrelRollers.Data;
using TeamBarrelRollers.BrainMemory;
using TeamBarrelRollers.Utilities;
using System.Threading;
using System.Collections.Generic;

namespace TeamBarrelRollers
{
	public class Bot
	{
		private StreamWriter writer;

        private MultiControl Mcontrol = new MultiControl();

		StreamReader reader;

		int currentTick = 0;

		/// <summary>
		/// Base initializer. Does not join any games.
		/// </summary>
		/// <param name="reader">Reader.</param>
		/// <param name="writer">Writer.</param>
		Bot (StreamReader reader, StreamWriter writer)
		{
			this.writer = writer;
			this.reader = reader;
		}

		public Bot(StreamReader reader, StreamWriter writer, BotId id, string track, string password, int carcount, bool host) : this(reader, writer)
		{
			if(host)
			{
				CreateRace create = new CreateRace (id, password, track, carcount);
				send (create);
			}

			Thread.Sleep (100);

			JoinRace join = new JoinRace (id, password, track, carcount);
			send (join);

			Run ();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TeamBarrelRollers.Bot"/> class. Normal join method used.
		/// </summary>
		/// <param name="reader">Reader.</param>
		/// <param name="writer">Writer.</param>
		/// <param name="join">Join.</param>
		public Bot(StreamReader reader, StreamWriter writer, Join join) : this(reader, writer)
		{
			send(join);

			Run ();
		}

		public void Run() {
		
			//Console.WriteLine ("Waiting for game to begin");

			string line;

			while((line = reader.ReadLine()) != null) {
				MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);

				if (msg.gameTick != null) {
					currentTick = (int)msg.gameTick;
				}

				switch(msg.msgType) {
				    case "yourCar":
					    YourBotWrapper bot = JsonConvert.DeserializeObject<YourBotWrapper> (line);
                        Mcontrol.Start(bot.Data);
					    break;

				    case "raceSession":
					    sendPing ();
					    break;

				    case "gameInit":
					    RaceWrapperWrapper racewrapper = JsonConvert.DeserializeObject<RaceWrapperWrapper> (line);
                        this.Mcontrol.New();
                        this.Mcontrol.Init(racewrapper.Data.Data);
					    sendPing ();
					    break;

                    case "gameStart":
                        this.Mcontrol.OurBot.CarThrottle = 1;
                        sendPing();
                        break;

                    case "carPositions":
                        CarPositions positions = JsonConvert.DeserializeObject<CarPositions>(line);
                        Mcontrol.Update(positions);
                        send(Mcontrol.UpdateReply());
                        break;

                    case "crash":
                        CrashWrapper crashwrap = JsonConvert.DeserializeObject<CrashWrapper>(line);
                        Mcontrol.Crash(crashwrap);
                        break;

                    case "turboAvailable":
                        Mcontrol.TurboGet(JsonConvert.DeserializeObject<TurboWrapper>(line));
                        break;

				    case "gameEnd":
					    sendPing ();
					    break;

				    default:
                        send(new Throttle(1));
					    break;
				}
			}
		}

		void sendPing()
		{
			send(new Ping());
		}

		private void send(SendMsg msg) {
			writer.WriteLine(msg.ToJson());
		}
	}
}

