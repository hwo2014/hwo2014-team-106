﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamBarrelRollers.Utilities
{
    public static class SwitcherSolver
    {
        public static string SolveSwitch(int _currentlaneindex, int _wantedlaneindex)
        {
            if (_currentlaneindex > _wantedlaneindex)
            {
                return "Left";
            }
            else if(_currentlaneindex < _wantedlaneindex)
            {
                return "Right";
            }
            else
            {
                return "";
            }
        }
    }
}
