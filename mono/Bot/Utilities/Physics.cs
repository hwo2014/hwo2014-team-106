﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace TeamBarrelRollers.Utilities
{
    public static class Physics
    {
        /// <summary>
        /// Returns the Centripetal Acceleration directing outwards from the circle
        /// </summary>
        /// <param name="speed">Speed of the car</param>
        /// <param name="radius">Radius of the circle</param>
        /// <returns>Centripetal Acceleration as m/s^2</returns>
        public static double CentripetalAcceleration(double speed, double radius)
        {
            if (radius < 0.00001)
                return 0;
            else
            return Math.Pow(speed, 2)/radius;
        }

        /// <summary>
        /// Returns Centripetal Force directing outwards from the circle
        /// </summary>
        /// <param name="centripetalAcceleration">Centripetal Acceleration of the car</param>
        /// <param name="mass">Mass of the car</param>
        /// <returns>Returns Centripal Force as N</returns>
        public static double CentripetalForce(double centripetalAcceleration, double mass)
        {
            return mass * centripetalAcceleration;
        }

        /// <summary>
        /// Returns the maximum speed that can be obtained with known maximum centripetal acceleration.
        /// </summary>
        /// <param name="maxCentripetalAcceleration"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public static double MaxCornerSpeed(double maxCentripetalAcceleration, double radius)
        {
            return Math.Sqrt(maxCentripetalAcceleration) * Math.Sqrt(radius);
        }

        /// <summary>
        /// Returns the Acceleration
        /// </summary>
        /// <param name="velocityChange">Delta of velocity</param>
        /// <param name="timeChange">Delta of time</param>
        /// <returns>Acceleration in m/s^2</returns>
        public static double Acceleration(double velocityChange, double timeChange)
        {
            return velocityChange/timeChange;
        }

        /// <summary>
        /// Returns the Force of Acceleration
        /// </summary>
        /// <param name="acceleration">Velocity if the car</param>
        /// <param name="mass">Mass of the car</param>
        /// <returns>Acceleration in m/s^2</returns>
        public static double AccelerationForce(double acceleration, double mass)
        {
            return acceleration / mass;
        }

        /// <summary>
        /// Returns the final velocity of current acceleration*time.
        /// </summary>
        /// <param name="currentVelocity">Current velocity</param>
        /// <param name="acceleration">Acceleration</param>
        /// <param name="time">Time</param>
        /// <returns>Final Velocity</returns>
        public static double FinalVelocity(double currentVelocity, double acceleration, double time)
        {
            return currentVelocity + acceleration*time;
        }

        /// <summary>
        /// Solves the acceleration needed for desired final velocity
        /// </summary>
        /// <param name="currentVelocity">Current velocity</param>
        /// <param name="finalVelocity">Wanted velocity</param>
        /// <param name="time">Time in wich the change has to be done</param>
        /// <returns></returns>
        public static double AccelerationForFinalVelocity(double currentVelocity, double finalVelocity, double time)
        {
            return (finalVelocity - currentVelocity)/time;
        }

        /// <summary>
        /// Estimates the time that will take to reach the target velocity with linear acceleration.
        /// </summary>
        /// <param name="currentVelocity"></param>
        /// <param name="finalVelocity"></param>
        /// <param name="acceleration"></param>
        /// <returns></returns>
        public static double EstimatedTimeofArrival(double currentVelocity, double finalVelocity, double acceleration)
        {
            return (finalVelocity - currentVelocity)/acceleration;
        }

        /// <summary>
        /// This is magic.
        /// </summary>
        /// <param name="spell">Enter the Spell in doublish</param>
        /// <returns></returns>
        public static double Magic(double spell)
        {
            double X4 = double.Parse("2.735E-8", CultureInfo.InvariantCulture);
            double X3 = double.Parse("1.62241E-6", CultureInfo.InvariantCulture);
            double X2 = 0.000964109;
            double X1 = 0.16484;
            double C = 9.19882;

            return -X4*Math.Pow(spell, 4) + X3*Math.Pow(spell, 3) + X2*Math.Pow(spell, 2) - X1*spell + C;
        }

        public static double DistanceToDecelerate(double currentSpeed, double wantedSpeed)
        {
            double totalDistance = 0;
            double counter = SpeedTicks(currentSpeed);
            while (counter < SpeedTicks(wantedSpeed))
            {
                totalDistance += Magic(counter);
                counter++;
            }

            return totalDistance;
        }

        public static double SpeedTicks(double currentSpeed)
        {
            int ones;
            int tenths;
            int hundreths;
            if (Magic(0) > currentSpeed)
            {
                ones = 0;
                tenths = 10;
                hundreths = 100;
                while (Magic(ones) > currentSpeed)
                {
                    ones++;
                }
                while (Magic(ones + tenths*0.1) < currentSpeed)
                {
                    tenths--;
                }
                while (Magic(ones + tenths*0.1 + hundreths*0.01) > currentSpeed)
                {
                    hundreths++;
                }

            }
            else
            {
                ones = 0;
                tenths = 0;
                hundreths = 100;
                while (Magic(ones) < currentSpeed)
                {
                    ones--;
                }
                while (Magic(ones + tenths * 0.1) > currentSpeed)
                {
                    tenths++;
                }
                while (Magic(ones + tenths * 0.1 + hundreths * 0.01) < currentSpeed)
                {
                    hundreths--;
                }
            }
            return ones + tenths*0.1 + hundreths*0.01;
        }
    }
}
