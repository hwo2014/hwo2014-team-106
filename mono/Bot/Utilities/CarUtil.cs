﻿using System;
using TeamBarrelRollers.Data;

namespace TeamBarrelRollers.Utilities
{
	public static class CarUtil
	{
		public static int GetCurrentTrackPieceIndex(string color, CarPositions positions)
		{
			foreach (var car in positions.Positions) {
				if(car.ID.Color == color)
				{
					return car.PiecePosition.PieceIndex;
				}
			}

			return -1;
		}
	}
}

