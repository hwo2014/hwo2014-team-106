﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.Data;

namespace TeamBarrelRollers.Utilities
{
    static class TrackCalculator
    {
        public static Track track { get; set; }
        public static int GlobalTrackLane { get; private set; }

        public static void CalculateTrackGenericLane()
        {
            double tempTurns = CalculateLaneValue(0,track.Pieces.Count() - 1);

            GlobalTrackLane = (int)EvaluateLaneFromDegrees(tempTurns);
        }

        public static double CalculateLaneValue(int _startindex, int _endindex){

            double tempValue = 0.0;
            for (int i = _startindex; i < _endindex; i++ ) 
            {
                if (track.Pieces[i].Angle != null)
                {
                    tempValue += (double)track.Pieces[i].Angle;
                }
            }

            return tempValue;
        }

        public static int? EvaluateLaneFromDegrees(double degrees)
        {
            int? temp = null;
            if(degrees < 0){
                temp = 0;
            }
            else if( degrees > 0)
            {
                temp = track.Lanes[track.Lanes.Count()-1].Index;
            }

            return temp;
        }
    }
}
