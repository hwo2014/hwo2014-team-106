﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamBarrelRollers.Utilities
{
    class Vec
    {
        /// <summary>The X coordinate.</summary>
        public double X;

        /// <summary>The Y coordinate.</summary>
        public double Y;

        /// <summary>Constructs the vector</summary>
        /// <param name="x">The X coordinate.</param>
        /// <param name="y">The Y coordinate.</param>
        public Vec(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Rotates the vektorr
        /// </summary>
        /// <param name="angle">Angle in degrees</param>
        /// <param name="vector">Vector to be turnd</param>
        /// <param name="result">Vector dat is tunrd</param>
        public static Vec rotate(double angle, Vec vector)
        {

            Vec result = new Vec(0, 0);
            angle = angle*Math.PI/180;
            double cs = Math.Cos(angle);
            double sn = Math.Sin(angle);
            double tx = vector.X * cs - vector.Y * sn;
            double ty = vector.X * sn + vector.Y * cs;
            result.X = tx;
            result.Y = ty;
            return result;
        }
    }
}
