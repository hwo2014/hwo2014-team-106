﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
    public class Race
    {
		[JsonProperty(PropertyName = "raceSession")]
		public RaceSession Session { get; set; }

        [JsonProperty(PropertyName = "track")]
		public Track Track { get; set; }

        [JsonProperty(PropertyName = "cars")]
		public List<Car> Cars { get; set; }

        public void Init()
        {
            this.Track.InitPieces();
        }
    }

	public class RaceSession
    {
        [JsonProperty(PropertyName = "laps")]
        public int Laps { get; set; }

        [JsonProperty(PropertyName = "maxLapTimeMs")]
        public int MaxLapTime { get; set; }

        [JsonProperty(PropertyName = "quickRace")]
        public bool QuickRace { get; set; }
    }
}
