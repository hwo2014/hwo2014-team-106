﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
    public class CarPositionLane
    {
        [JsonProperty(PropertyName = "startLaneIndex")]
        public int StartLaneIndex { get; set; }

        [JsonProperty(PropertyName = "endLaneIndex")]
        public int EndLaneIndex { get; set; }
    }
}
