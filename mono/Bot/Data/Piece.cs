﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
    public class Piece
    {
        public PieceType Type;

		[JsonProperty(PropertyName = "length")]
		public double? Length { get; set; }

        [JsonProperty(PropertyName = "switch")]
		public bool? Switchable { get; set; }

		[JsonProperty(PropertyName = "radius")]
		public int? Radius { get; set; }

		[JsonProperty(PropertyName = "angle")]
		public double? Angle { get; set; }

    }

    public enum PieceType
    {
        Straight,
        StraightSwitcher,
        Turn,
        TurnSwitcher
    }
}
