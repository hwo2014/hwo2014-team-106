﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
    public class Track
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "pieces")]
		public List<Piece> Pieces { get; set; }

        [JsonProperty(PropertyName = "lanes")]
		public List<Lane> Lanes { get; set; }

		[JsonProperty(PropertyName = "startingPoint")]
		public StartingPoint StartingPoint { get; set; }

        public void InitPieces()
        {
            foreach (Piece piece in this.Pieces)
            {
                if(piece.Length != null){
				    piece.Type = PieceType.Straight;
                }

                if (piece.Angle != null)
                {
                    piece.Type = PieceType.Turn;
                }

                if(piece.Switchable != null){
                    if (piece.Length != null)
                    {
                        piece.Type = PieceType.StraightSwitcher;
                    }
                    else
                    {
                        piece.Type = PieceType.TurnSwitcher;
                    }
                }
                

            }
        }
    }
}
