﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
	public class CarPositions
	{
		[JsonProperty(PropertyName = "msgType")]
		public string MsgType {
			get;
			set;
		}

		[JsonProperty(PropertyName = "gameId")]
		public string GameID {
			get;
			set;
		}

		[JsonProperty(PropertyName = "gameTick")]
		public int GameTick {
			get;
			set;
		}

		[JsonProperty(PropertyName = "data")]
		public List<CarPosition> Positions {
			get;
			set;
		}
	}
}

