﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
    public class CrashWrapper
    {
        [JsonProperty(PropertyName = "msgType")]
        public string MsgType { get; set; }

        [JsonProperty(PropertyName = "data")]
        public CrashData Data { get; set; }

        [JsonProperty(PropertyName = "gameID")]
        public string GameID { get; set; }

        [JsonProperty(PropertyName = "gameTick")]
        public int GameTick { get; set; }
    }
}
