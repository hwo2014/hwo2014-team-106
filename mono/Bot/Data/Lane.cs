﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
    public class Lane
    {
		[JsonProperty(PropertyName = "index")]
		public int Index { get; set; }

		[JsonProperty(PropertyName = "distanceFromCenter")]
		public int DistanceFromCenter { get; set; }
    }
}
