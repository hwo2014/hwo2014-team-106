﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
    public class StartingPoint
    {
        [JsonProperty(PropertyName = "position")]
        public Position position { get; set; }

		[JsonProperty(PropertyName = "angle")]
        public double angle { get; set; }
    }

	public class Position
    {
		[JsonProperty(PropertyName = "x")]
        public double x { get; set; }

		[JsonProperty(PropertyName = "y")]
        public double y { get; set; }
    }
}
