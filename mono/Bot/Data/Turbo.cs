﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
    public class Turbo
    {
        [JsonProperty(PropertyName = "turboDurationMilliseconds")]
        public double TurboMs { get; set; }

        [JsonProperty(PropertyName = "turboDurationTicks")]
        public int TurboTicks { get; set; }

        [JsonProperty(PropertyName = "turboFactor")]
        public double Factor { get; set; }
    }
}
