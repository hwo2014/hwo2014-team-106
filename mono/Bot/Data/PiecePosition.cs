﻿using System;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
	public class PiecePosition
	{
		[JsonProperty(PropertyName = "pieceIndex")]
		public int PieceIndex {
			get;
			set;
		}

		[JsonProperty(PropertyName = "inPieceDistance")]
		public double InPieceDistance {
			get;
			set;
		}

		[JsonProperty(PropertyName = "lane")]
		public CarPositionLane Lane {
			get;
			set;
		}

		[JsonProperty(PropertyName = "lap")]
		public int Lap {
			get;
			set;
		}
	}
}

