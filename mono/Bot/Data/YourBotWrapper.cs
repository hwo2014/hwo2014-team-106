﻿using System;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
	public class YourBotWrapper
	{
		[JsonProperty(PropertyName = "msgType")]
		public string MsgType {
			get;
			set;
		}

		[JsonProperty(PropertyName = "data")]
		public YourBot Data {
			get;
			set;
		}
	}
}

