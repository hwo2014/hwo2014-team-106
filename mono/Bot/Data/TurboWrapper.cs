﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
    public class TurboWrapper
    {
        [JsonProperty(PropertyName = "msgType")]
        public string MsgType { get; set; }

        [JsonProperty(PropertyName = "data")]
        public Turbo Data { get; set; }
    }
}
