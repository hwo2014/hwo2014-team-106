﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
	public class Car
	{
	    [JsonProperty(PropertyName = "id")]
		public CarId Id { get; set; }

	    [JsonProperty(PropertyName = "dimensions")]
		public CarDimensions Dimensions { get; set; }
	}

	public class CarId
	{
	    [JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

	    [JsonProperty(PropertyName = "color")]
		public string Color { get; set; }
	}

	public class CarDimensions
	{
		[JsonProperty(PropertyName = "length")]
		public double Length { get; set; }

	    [JsonProperty(PropertyName = "width")]
		public double Width { get; set; }

		[JsonProperty(PropertyName = "guideFlagPosition")]
		public double GuideFlagPosition { get; set; }
	}
}
