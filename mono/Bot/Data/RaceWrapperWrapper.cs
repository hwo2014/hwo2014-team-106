﻿using System;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
	public class RaceWrapperWrapper
	{
		[JsonProperty(PropertyName = "msgType")]
		public string MsgType {
			get;
			set;
		}

		[JsonProperty(PropertyName = "data")]
		public RaceWrapper Data {
			get;
			set;
		}
	}
}

