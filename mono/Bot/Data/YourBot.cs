﻿using System;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{
	public class YourBot
	{
		[JsonProperty(PropertyName = "name")]
		public string Name {
			get;
			set;
		}

		[JsonProperty(PropertyName = "color")]
		public string Color {
			get;
			set;
		}
	}
}

