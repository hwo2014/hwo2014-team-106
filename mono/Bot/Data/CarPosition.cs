﻿using System;
using Newtonsoft.Json;

namespace TeamBarrelRollers.Data
{

	public class CarPosition
	{
		[JsonProperty(PropertyName = "id")]
		public CarId ID {
			get;
			set;
		}

		[JsonProperty(PropertyName = "angle")]
		public double Angle {
			get;
			set;
		}

		[JsonProperty(PropertyName = "piecePosition")]
		public PiecePosition PiecePosition {
			get;
			set;
		}
	}
}

