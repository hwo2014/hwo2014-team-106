﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamBarrelRollers.Net
{
    public class SwitchMsg : SendMsg
    {
        public string value;

        public SwitchMsg(string value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}
