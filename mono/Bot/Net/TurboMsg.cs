﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamBarrelRollers.Net
{
    class TurboMsg : SendMsg
    {
        public string value;

        public TurboMsg(string value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "turbo";
        }
    }
}
