﻿using System;
using TeamBarrelRollers.Data;

namespace TeamBarrelRollers.Net
{
	public class CreateRace : SendMsg
	{
		public BotId botId;
		public string trackName;
		public string password;
		public int carCount;

		public CreateRace (BotId botId, string password, string track, int cars)
		{
			this.botId = botId;
			this.trackName = track;
			this.password = password;
			this.carCount = cars;
		}

		#region implemented abstract members of SendMsg

		protected override string MsgType ()
		{
			return "createRace";
		}

		#endregion

	}
}

