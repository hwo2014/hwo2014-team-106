﻿using System;

namespace TeamBarrelRollers.Net
{
	public class MsgWrapper {
		public string msgType;
		public int? gameTick;
		public Object data;

		public MsgWrapper(string msgType, Object data) {
			this.msgType = msgType;
			this.data = data;
		}
	}
}

