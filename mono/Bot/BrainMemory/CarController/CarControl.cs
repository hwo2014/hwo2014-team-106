﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.BrainMemory.Switcher;
using TeamBarrelRollers.BrainMemory.Properties;
using TeamBarrelRollers.Utilities;

namespace TeamBarrelRollers.BrainMemory.CarController
{
    public class CarControl
    {
        public BotCar OurBot;
        public TrackControl TControl;
        public SwitchControl SControl;
        public bool goOnce;
        public int Lap;

        public CarControl(BotCar _ourbot, TrackControl _tcontr, SwitchControl _scontr)
        {
            this.OurBot = _ourbot;
            this.TControl = _tcontr;
            this.SControl = _scontr;
        }

        internal void Init()
        {
            this.TControl.setDefaultThrottle(1);
            Lap = 1;
            goOnce = true;
        }

        internal void Update()
        {
            
            if (false == true && goOnce)
            {
                Lap++;
                goOnce = false;
            }
            if (OurBot.OnPiece.Index == 2)
                goOnce = true;
            if (Lap >= 3 && OurBot.OnPiece.Index==27)
            {
                OurBot.TurboEngage("Gylläh!");
                OurBot.CarThrottle = 1;

            }
            Vec vector = new Vec(1,0);
            //Usasettings
            /*if (OurBot.TurnState == CarTrackState.EnteringTurn && OurBot.Speed > 109.21)
                OurBot.CarOptionalThrottle = 0.7;
            else if (OurBot.TurnState == CarTrackState.OnTurn && OurBot.Speed > 81.31)
            {
                OurBot.CarOptionalThrottle = 0.7;
            }
            else
            {
                /*vector = Vec.rotate(Math.Abs(OurBot.Angle)*(3/2), vector);
                OurBot.CarOptionalThrottle = Math.Abs(Math.Pow(vector.X, 2));
                OurBot.CarOptionalThrottle = 1;
           }*/
            //Germanysettings
            /*Physics.MaxCornerSpeed(0.469, OurBot.CurrentLane.LaneRadius)*/
            /*NextTurnInfo info = TControl.GetNextTurn(OurBot);
            double MaxCornerSpeed = Physics.MaxCornerSpeed(0.4, TControl.Track[info.NextTurnIndex].Angle);

            if (OurBot.TurnState != CarTrackState.OnTurn)
            {
                if (OurBot.Speed > MaxCornerSpeed && info.LengthToTurn < Physics.DistanceToDecelerate(OurBot.Speed, MaxCornerSpeed))
                    OurBot.CarOptionalThrottle = 0;
                    else
                    OurBot.CarOptionalThrottle = 1;
            }
            else
            {
                if(MaxCornerSpeed > 1)
                OurBot.CarOptionalThrottle = MaxCornerSpeed*0.1;
                else
                {
                    OurBot.CarOptionalThrottle = 1;
                }
            }
            if(OurBot.Speed > 8)*/
                OurBot.CarThrottle = 0.65;

        }

        public int GetNextIndex(int current, int max)
        {
            if (current > max)
                return 0;
            else
                return 1;
        }
    }
}
