﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.Data;
using TeamBarrelRollers.Utilities;

namespace TeamBarrelRollers.BrainMemory.Analyst
{
    public class Analyzer
    {
        MultiControl MControl;
        public double currentThrottle;

        public Analyzer(MultiControl _mcontr)
        {
            this.MControl = _mcontr;
        }

        public void Update() 
        {
            using (StreamWriter sw = new StreamWriter("log.txt", true))
            {
                NextTurnInfo info = MControl.CControl.TControl.GetNextTurn(MControl.CControl.OurBot);
                //sw.WriteLine ("-------------------------------");
                //sw.WriteLine (String.Format ("MControl: {0}", MControl));
                sw.WriteLine(String.Format("Acceleration;{0}", MControl.CControl.OurBot.Acceleration));
                sw.WriteLine(String.Format("Speed;{0}", MControl.CControl.OurBot.Speed));
                sw.WriteLine(String.Format("DistanceToDecelerate;{0}", Physics.DistanceToDecelerate(MControl.CControl.OurBot.Speed, 0.8)));
                sw.WriteLine(String.Format("LengthToTurn;{0}", MControl.CControl.TControl.GetNextTurn(MControl.CControl.OurBot).LengthToTurn));
                sw.WriteLine(String.Format("MaxCornerSpeed;{0}", Physics.MaxCornerSpeed(0.4, MControl.CControl.TControl.Track[info.NextTurnIndex].Angle)));
                sw.WriteLine(String.Format("Centripetal;{0}", MControl.CControl.OurBot.Centripetal));
            }
        }

        public void Crash(CrashWrapper _crashdata)
        {
            if (_crashdata.Data.Color == this.MControl.OurBot.Color)
            {

            }
        }

    }
}
