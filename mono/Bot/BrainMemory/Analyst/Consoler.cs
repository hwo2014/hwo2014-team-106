﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.BrainMemory;
using TeamBarrelRollers.Utilities;

namespace TeamBarrelRollers.BrainMemory.Analyst
{
    public static class Consoler
    {
        public static bool StatsEnabled;
        private static ConsoleSize Size;
        private static MultiControl MControl;

        static Consoler()
        {
            Size = new ConsoleSize(61,30);

			Size.X = Console.WindowWidth;
			Size.Y = Console.WindowHeight;

            Console.SetWindowSize(Size.X, Size.Y);

            Console.Clear();
        }

        public static void Init(MultiControl _mc)
        {
            MControl = _mc;
        }

        public static void Update()
        {
            Console.Clear();

            //Top, 0 <-> 2
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(2, 0);
            Console.Write("Tick:" + MControl.OurBot.CurrentTick);
            Console.ForegroundColor = ConsoleColor.White;

            Console.SetCursorPosition(Size.X - 17, 0);
            Console.Write("Botwid: " + MControl.OurBot.Dimensions.Width);
            Console.SetCursorPosition(Size.X - 17, 1);
            Console.Write("Botlen: " + MControl.OurBot.Dimensions.Length);
            //Data area, 4 <-> Size.Y - 4

            Console.SetCursorPosition(2, 4);
            Console.Write("CentripetalAcceleration: "+ Physics.CentripetalAcceleration(MControl.OurBot.Speed,MControl.OurBot.CurrentLane.LaneRadius).ToString());
            Console.SetCursorPosition(2, 5);
            Console.Write("Acceleration: "+ MControl.OurBot.Acceleration.ToString());
            Console.SetCursorPosition(2, 6);
            NextTurnInfo turninfo = MControl.Tcontrol.GetNextTurn(MControl.OurBot);
            Console.Write("Next turn: {0}",turninfo.LengthToTurn);

            //Bottom, Size.Y <-> 2 
            // Left
            Console.SetCursorPosition(2, Size.Y -2);
            Console.Write("Speed: " + MControl.OurBot.Speed.ToString("F2"));
            Console.SetCursorPosition(16, Size.Y - 2);
            Console.Write("Throttle "+ MControl.OurBot.CarThrottle.ToString("F2"));
            Console.SetCursorPosition(2, Size.Y - 1);
            Console.Write("Angle: "+ MControl.OurBot.Angle.ToString("F2"));

            // Right
            Console.SetCursorPosition(Size.X - 17, Size.Y - 2);
            Console.Write("OnPiece: " + MControl.OurBot.OnPiece.Index);    
            Console.SetCursorPosition(Size.X - 17, Size.Y - 1);
            Console.Write("Position: " + MControl.OurBot.OnPieceDistance.ToString("F2"));
        }
    }

    struct ConsoleSize
    {
        public int X;
        public int Y;

        public ConsoleSize(int _x, int _y)
        {
            this.X = _x;
            this.Y = _y;
        }
    }

    public enum ConsoleViewMode
    {
        Data,
        Events
    }
}
