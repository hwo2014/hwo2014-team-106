﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.Data;
using TeamBarrelRollers.BrainMemory;
using TeamBarrelRollers.BrainMemory.Properties;

namespace TeamBarrelRollers.BrainMemory
{
    public class TrackControl
    {
        public List<UniTrackPiece> Track;
        public List<UniLane> TrackLanes;

        public TrackControl()
        {
            this.Track = new List<UniTrackPiece>();
            this.TrackLanes = new List<UniLane>();
        }

        public void Init(Race _race)
        {
            foreach (Lane lane in _race.Track.Lanes)
            {
                this.TrackLanes.Add(new UniLane(_race.Track.Lanes.IndexOf(lane), 0.0, lane.DistanceFromCenter, 0));
            }

            foreach (Piece tp in _race.Track.Pieces)
            {
                UniTrackPiece tempUniPiece = new UniTrackPiece();
                tempUniPiece.Index = _race.Track.Pieces.IndexOf(tp);
                
                foreach (Lane lane in _race.Track.Lanes)
                {
                    tempUniPiece.Lanes.Add(new UniLane(_race.Track.Lanes.IndexOf(lane), 0.0, lane.DistanceFromCenter, 0));
                }

                tempUniPiece.Init(tp);
                this.Track.Add(tempUniPiece);
            }
        }

        public void setDefaultThrottle(double _throttle)
        {
            foreach (UniTrackPiece _piece in this.Track)
            {
                _piece.ThrottleValue = _throttle;
            }
        }

        internal NextTurnInfo GetNextTurn(BotCar _bot)
        {
            int pieceCount = 0;
            int turnindex = 0;
            double lengthtoturn = 0;

            //new version
            int startindex = _bot.OnPiece.Index;
            bool foundTurn = false;
            List<UniTrackPiece> turnsegment = new List<UniTrackPiece>();

            while (!foundTurn)
            {
                UniTrackPiece testpiece = this.GetPiece(startindex);

                if (testpiece.Type == PieceType.Straight || testpiece.Type == PieceType.StraightSwitcher)
                {
                    turnsegment.Add(testpiece);
                }
                else
                {
                    foundTurn = true;
                }

                startindex++;
                if (startindex >= this.Track.Count - 1)
                {
                    startindex = 0;
                }
            }
            
            pieceCount = turnsegment.Count;

            if(pieceCount > 0 && _bot.OnPiece.Type != PieceType.Turn && _bot.OnPiece.Type != PieceType.TurnSwitcher){
                turnindex = turnsegment.Last().Index + 1;

                foreach(UniTrackPiece piece in turnsegment)
                {
                    foreach (UniLane lane in piece.Lanes)
                    {
                        if (lane.Index == _bot.LaneOut && piece.Index != _bot.OnPiece.Index)
                        {
                            lengthtoturn += lane.Length;
                        }
                    } 
                }

                lengthtoturn += (_bot.OnPiece.Lanes[_bot.LaneOut].Length - _bot.OnPieceDistance);

            }
            else
            {
                lengthtoturn = 0.0;
                if (_bot.OnPiece.Type != PieceType.Turn && _bot.OnPiece.Type != PieceType.TurnSwitcher)
                {
                    lengthtoturn += (_bot.OnPiece.Lanes[_bot.LaneOut].Length - _bot.OnPieceDistance);
                }
                turnindex = _bot.OnPiece.Index;
                
            }

            return new NextTurnInfo(pieceCount, turnindex, lengthtoturn);
        }
   
        internal UniTrackPiece GetPiece(int _pieceIndex)
        {
            UniTrackPiece tempiece = new UniTrackPiece();
            foreach (UniTrackPiece piece in this.Track)
            {
                if (piece.Index == _pieceIndex)
                {
                    tempiece = piece;
                    break;
                }
            }

            return tempiece;
        }
    }

    internal struct NextTurnInfo
    {
        internal int PiecesToTurn;
        internal int NextTurnIndex;
        internal double LengthToTurn;

        internal NextTurnInfo(int _pieces, int _turnindex, double _length)
        {
            this.PiecesToTurn = _pieces;
            this.NextTurnIndex = _turnindex;
            this.LengthToTurn = _length;
        }
    }
}
