﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamBarrelRollers.BrainMemory.Properties
{
    public enum CarState
    {
        OnTrack,
        Crashed
    }

    public enum CarTrackState
    {
        GonnaEnterTurn,
        EnteringTurn,
        OnTurn,
        ExitingTurn,
        Straight
    }

    public enum TurboState
    {
        Off,
        Init,
        On
    }
}
