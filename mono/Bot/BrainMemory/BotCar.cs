﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.Data;
using TeamBarrelRollers.BrainMemory.Properties;
using TeamBarrelRollers.BrainMemory.Switcher;
using TeamBarrelRollers.Utilities;
using System.IO;

namespace TeamBarrelRollers.BrainMemory
{
    public class BotCar
    {
        public string Name { get; set; }
        public string Color { get; set; }

        public Techs Dimensions;

        // Current tick Variables
        public int CurrentTick = 0;
        public double Angle { get; set; }
        public double LastAngle { get; private set; }
        public UniTrackPiece OnPiece { get; private set; }
        public double OnPieceDistance { get; private set; }
        public int LaneIn { get; private set; }
        public int LaneOut { get; private set; }
        public UniLane CurrentLane { get; private set; }
        private double LastPieceDistance;
        private bool Switching;

        // Last tick Variables
        private int LastTick = 0;
        private UniTrackPiece LastTickPiece; 
        internal double SpeedLastTick { get; private set; }
        private UniLane LastTickLane;
        private int LastTickLaneIn;
        private int LastTickLaneOut;
        
        //Calculated Stuff
        public double Speed { get; private set; }
        public double Acceleration { get; set; }
        public double Centripetal { get; set; }

        public double CarThrottle = 1;
        internal double LastsentThrottle = 0.0;

        //Turbo

        public bool TurboAvailable { get; private set; }
        public TurboState TurboStatus { get; set; }
        public int TurboTicks { get; private set; }
        public int TurboRemainingTicks { get; private set; }
        public double TurboFactor { get; private set; }
        public string TurboTaunt { get; private set; }

        //Switch
        internal SwitchSegment currentSegment;
        private bool firstUpdate = true;

        public BotCar(string _name, string _color)
        {
            this.Name = _name;
            this.Color = _color;
            this.LaneIn = 0;
            this.LaneOut = 0;
            this.Speed = 0.0;
            this.LastAngle = 0.0;
            this.Acceleration = 0.0;
            this.Centripetal = 0.0;

            //Turbo

            TurboAvailable = false;
            TurboStatus = TurboState.Off;
            TurboTicks = 0;
            TurboRemainingTicks = 0;
            TurboFactor = 0.0;
        }

        public void Init(Race _race, TrackControl _tcontrol)
        {
            this.OnPiece = _tcontrol.GetPiece(0);

            this.LastTickPiece = this.OnPiece;
            this.LastTickLaneOut = 0;
            this.LastTickLaneIn = 0;

            this.CurrentTick = 0;
            this.LastTick = 0;

            foreach (Car car in _race.Cars)
            {
                if (car.Id.Color == this.Color)
                {
                    this.Dimensions = new Techs(car.Dimensions.Length, car.Dimensions.Width, car.Dimensions.GuideFlagPosition);
                }
            }
        }

        public void Update(CarPositions _pos, TrackControl _track)
        {
            try
            {
                //--START-- TickSet and Turbo
                if (this.CurrentTick != _pos.GameTick)
                {
                    this.LastTick = this.CurrentTick;
                    this.CurrentTick = _pos.GameTick;

                    if (this.TurboStatus == TurboState.On)
                    {
                        this.TurboRemainingTicks--;

                        if (this.TurboRemainingTicks == 0)
                        {
                            this.TurboStatus = TurboState.Off;
                        }
                    }
                }
                //--END--
            }
            catch (Exception e)
            {
                throw e;
            }
            foreach (CarPosition _posi in _pos.Positions)
            {
                //--START-- Tick init
                #region TickInit
                if (_posi.ID.Color == this.Color)
                {
                    this.LastAngle = this.Angle;
                    this.Angle = _posi.Angle;
                    UniTrackPiece tempPiece = new UniTrackPiece();

                    tempPiece = _track.GetPiece(_posi.PiecePosition.PieceIndex);

                    if (this.OnPiece.Index != tempPiece.Index)
                    {
                        this.LastTickPiece = this.OnPiece;
                        this.LastTickLane = LastTickPiece.Lanes[this.LaneOut];
                        this.OnPiece = tempPiece;

                    }
                    else
                    {
                        this.OnPiece = tempPiece;
                        this.LastTickPiece = this.OnPiece;
                        this.LastTickLane = LastTickPiece.Lanes[this.LaneOut];
                    }

                    this.LastPieceDistance = this.OnPieceDistance;
                    this.OnPieceDistance = _posi.PiecePosition.InPieceDistance;

                    this.LastTickLaneIn = this.LaneIn;
                    this.LastTickLaneOut = this.LaneOut;

                    this.LaneIn = _posi.PiecePosition.Lane.StartLaneIndex;
                    this.LaneOut = _posi.PiecePosition.Lane.EndLaneIndex;

                    if (this.LaneIn != this.LaneOut)
                    {
                        if (!firstUpdate)
                        {
                            Switching = true;
                        }
                        else
                        {
                            firstUpdate = false;
                        }
                    }

                    this.CurrentLane = this.OnPiece.Lanes[this.LaneIn];
                }
                #endregion
                //--END--

                //--START-- Speed Calculation
                #region Speedcalculation
                this.SpeedLastTick = this.Speed;
                if (this.CurrentTick != 0 && this.CurrentTick != this.LastTick)
                {
                    double templanelength = 0.0;
                    double tempDistance = 0.0;

                    if (this.OnPiece.Index == this.LastTickPiece.Index)
                    {

                        tempDistance = this.OnPieceDistance - this.LastPieceDistance;
                        this.Speed = tempDistance / (this.CurrentTick - this.LastTick);

                    }
                    else
                    {
                        UniLane tempLane = new UniLane();

                        tempLane = this.LastTickPiece.GetUniLane(this.LastTickLane.Index);

                        double templength = tempLane.Length;

                        if (!Switching && LastTickLaneIn == LastTickLaneOut)
                        {
                            templength = tempLane.Length;
                        }
                        else if(Switching && LastTickLaneIn != LastTickLaneOut)
                        {
                            templength = this.LastTickPiece.GetSwitchLength(LastTickLaneIn, LastTickLaneOut);
                            Switching = false;
                        }

                        templanelength = templength - this.LastPieceDistance;
                        this.Speed = (templanelength + this.OnPieceDistance) / (this.CurrentTick - this.LastTick);

                    }
                }
                #endregion
                //--END--

                this.Acceleration = this.Speed - this.SpeedLastTick;
                Centripetal = Physics.CentripetalAcceleration(Speed, CurrentLane.LaneRadius);

            }
        }

        public void GetTurbo(TurboWrapper _turbo)
        {
            this.TurboTicks = _turbo.Data.TurboTicks;
            this.TurboRemainingTicks = this.TurboTicks;
            this.TurboFactor = _turbo.Data.Factor;
            this.TurboStatus = TurboState.Off;
            this.TurboAvailable = true;
        }

        public void TurboEngage(string TurboMsg)
        {
            if (this.TurboAvailable == true)
            {
                this.TurboTaunt = TurboMsg;
                this.TurboStatus = TurboState.Init;
            }
        }
    }

    public struct Techs
    {
        public double Length;
        public double Width;
        public double GuidePosition;

        public Techs( double _len, double _wid, double _guide)
        {
            Length = _len;
            Width = _wid;
            GuidePosition = _guide;
        }
    }
}
