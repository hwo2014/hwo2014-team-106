﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.Data;

namespace TeamBarrelRollers.BrainMemory.Switcher
{
    public class SwitchControl
    {
        TrackControl TControl;
        List<UniTrackPiece> SwitchPieces;
        internal List<SwitchSegment> Segments;
        internal List<BotCar> Cars;
        BotCar OurBot;

        public SwitchControl(TrackControl _tc)
        {
            this.Segments = new List<SwitchSegment>();
            this.TControl = _tc;
            this.SwitchPieces = new List<UniTrackPiece>();
            this.Cars = new List<BotCar>();
        }

        public void SetOurBot(BotCar _ourbot)
        {
            this.OurBot = _ourbot;
        }

        public void SetCars(List<BotCar> _cars)
        {
            this.Cars = _cars;
        }

        public void Init(Race _race)
        {
            foreach(UniTrackPiece piece in TControl.Track)
            {
                if (piece.Switchable == true)
                {
                    this.SwitchPieces.Add(piece);
                }
            }

            for (int i = 0; i < this.SwitchPieces.Count(); i++)
            {
                //kootaan segmentti
                List<UniTrackPiece> tempSegment = new List<UniTrackPiece>();

                if (i + 1 < this.SwitchPieces.Count())
                {
                    foreach (UniTrackPiece piece in TControl.Track)
                    {
                        if (piece.Index > this.SwitchPieces[i].Index && piece.Index < this.SwitchPieces[i + 1].Index)
                        {
                            tempSegment.Add(piece);
                        }
                    }
                }
                else
                {
                    foreach (UniTrackPiece piece in TControl.Track)
                    {
                        if (piece.Index > this.SwitchPieces[i].Index || piece.Index < this.SwitchPieces[0].Index)
                        {
                            tempSegment.Add(piece);
                        }
                    }
                }

                this.Segments.Add(new SwitchSegment(tempSegment));

                double tempLaneprefLength = Double.MaxValue;
                int SegmentLaneout = 0;

                foreach (UniLane lane in this.TControl.TrackLanes)
                {
                    double SegmentLanelength = 0.0;

                    foreach (UniTrackPiece tempsegpiece in tempSegment)
                    {
                        SegmentLanelength += tempsegpiece.Lanes[lane.Index].Length;
                    }

                    if (SegmentLanelength == tempLaneprefLength)
                    {
                        if (i > 0)
                        {
                            SegmentLaneout = this.SwitchPieces[i - 1].LaneOut;
                        }
                    }
                    else if (SegmentLanelength < tempLaneprefLength)
                    {
                        tempLaneprefLength = SegmentLanelength;
                        SegmentLaneout = lane.Index;
                    }
                }

                this.SwitchPieces[i].LaneOut = SegmentLaneout;
            }
        }

        public void Update(CarPositions _pos)
        {
            foreach (SwitchSegment segment in this.Segments)
            {
                foreach (CarPosition botcar in _pos.Positions)
                {
                    if (segment.CarOnSegment(botcar.PiecePosition.PieceIndex))
                    {
                        foreach (BotCar car in this.Cars)
                        {
                            if (car.Color == botcar.ID.Color)
                            {
                                car.currentSegment = segment;
                                break;
                            }
                        }
                    }
                }
            }
            OurBot.currentSegment.Update(_pos);
        }

        internal int getSwitchIndex(BotCar bot)
        {
            int switchat = -1;
            foreach(UniTrackPiece switcher in this.SwitchPieces)
            {
                if (switcher.Index > bot.OnPiece.Index && (switchat > switcher.Index || switchat == -1))
                {
                    switchat = switcher.Index;
                }
            }

            return switchat;
        }
    }
}

