﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.BrainMemory;
using TeamBarrelRollers.Data;

namespace TeamBarrelRollers.BrainMemory.Switcher
{
    class SwitchSegment
    {
        internal List<UniTrackPiece> Pieces;
        internal int SegmentStart { get; private set; }
        internal int SegmentEnd { get; private set; }
        internal List<SegmentLane> Lanes;

        internal SwitchSegment(List<UniTrackPiece> _segment)
        {
            this.Lanes = new List<SegmentLane>();
            this.Pieces = new List<UniTrackPiece>();
            this.Pieces = _segment;
            this.SegmentStart = -1;
            this.SegmentEnd = -1;
        }

        internal void Init()
        {
            foreach(UniLane lane in this.Pieces[0].Lanes)
            {
                this.Lanes.Add(new SegmentLane(lane.Index));
            }

            foreach (UniTrackPiece piece in this.Pieces)
            {
                if (piece.Index > this.SegmentEnd)
                {
                    this.SegmentEnd = piece.Index;
                }

                if (this.SegmentStart == -1)
                {
                    piece.Index = this.SegmentStart;
                }
                else
                {
                    if (piece.Index < this.SegmentStart)
                    {
                        this.SegmentStart = piece.Index;
                    }
                }

                foreach (SegmentLane slane in this.Lanes)
                {
                    foreach (UniLane lane in piece.Lanes)
                    {
                        if(lane.Index == slane.Index) 
                        {
                            slane.setLength(slane.Length + lane.Length);
                        }
                    }
                }
            }
            this.SegmentStart = this.SegmentStart - 1;
        }

        internal void Update(CarPositions _pos)
        {

        }

        internal bool CarOnSegment(int carIndex)
        {
            bool test = false;

            if (SegmentStart < SegmentEnd)
            {
                if (carIndex >= SegmentStart && carIndex <= SegmentEnd)
                {
                    test = true;
                }
            }
            else
            {
                if (carIndex >= SegmentStart || carIndex <= SegmentEnd)
                {
                    test = true;
                }
            }

            return test;
        }
    }
}
