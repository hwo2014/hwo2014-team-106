﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TeamBarrelRollers.BrainMemory.Switcher
{
    class SegmentLane
    {
        internal double Length { get; private set; }
        internal int Index { get; private set; }
        internal bool HasCar { get; private set; }
        internal double CarPosition { get; private set; }

        internal SegmentLane(int _ind)
        {
            this.Index = _ind;
        }

        internal void setLength(double _newlen)
        {
            this.Length = _newlen;
        }
    }
}
