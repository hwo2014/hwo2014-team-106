﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.Data;

namespace TeamBarrelRollers.BrainMemory
{
    public class UniTrackPiece
    {
        //Trackpiece
        public int Index;

        public PieceType Type;
        public double Angle;
        public int Radius;
        public double Length;
        public bool Switchable;

        //Switch
        public List<UniLane> Lanes;
        public int LaneOut;
        internal bool ForceSwitch;
        internal int ForceLaneOut;

        //Throttle
        public double DesiredSpeed;
        public double ThrottleValue;

        public UniTrackPiece()
        {
            this.Index = 0;
            this.Lanes = new List<UniLane>();
            this.Angle = 0.0;
            this.Radius = 0;
            this.Length = 0.0;
            this.Switchable = false;
            this.ForceSwitch = false;
        }

        public void Init(Piece _piece)
        {
            if(_piece.Angle != null){
                this.Angle = (double)_piece.Angle;
            }

            if (_piece.Radius != null)
            {
                this.Radius = (int)_piece.Radius;
            }

            if (_piece.Length != null)
            {
                this.Length = (double)_piece.Length;
            }

            if (_piece.Switchable != null)
            {
                this.Switchable = (bool)_piece.Switchable;
            }

            if (this.Length != 0.0)
            {
                this.Type = PieceType.Straight;
            }

            if (this.Angle != 0.0)
            {
                this.Type = PieceType.Turn;
            }

            if (this.Switchable != false)
            {
                if (this.Length != 0.0)
                {
                    this.Type = PieceType.StraightSwitcher;
                }
                else
                {
                    this.Type = PieceType.TurnSwitcher;
                }
            }

            if (this.Type == PieceType.Turn || this.Type == PieceType.TurnSwitcher)
            {
                this.Length = (double)(Math.Abs(this.Angle) * Math.PI * this.Radius) / 180;

                foreach (UniLane lane in this.Lanes)
                {
                    if (this.Angle < 0)
                    {
                        lane.LaneRadius = this.Radius + lane.DistanceFromCenter;
                        lane.Length = (double)(Math.Abs(this.Angle) * Math.PI * (lane.LaneRadius)) / 180;
                        
                    }
                    else
                    {
                        lane.LaneRadius = this.Radius - lane.DistanceFromCenter;
                        lane.Length = (double)(Math.Abs(this.Angle) * Math.PI * (lane.LaneRadius)) / 180;
                    }
                }
            }
            else
            {
                foreach (UniLane lane in this.Lanes)
                {
                    lane.Length = this.Length;
                }
            }
        }

        public double GetSwitchLength(int lanein, int laneout)
        {
            double tempLength = 0.0;
            UniLane lane1 = this.GetUniLane(lanein);
            UniLane lane2 = this.GetUniLane(laneout);

            int radiusDifference = 0;

            if (lane1.Length > lane2.Length)
            {
                radiusDifference = lane1.LaneRadius - lane2.LaneRadius;
                tempLength = Math.Sqrt(Math.Pow(radiusDifference, 2) + Math.Pow(lane2.Length, 2));

            }
            else if (lane1.Length < lane2.Length)
            {
                radiusDifference = lane2.LaneRadius - lane1.LaneRadius;
                tempLength = Math.Sqrt(Math.Pow(radiusDifference, 2) + Math.Pow(lane1.Length, 2));
            }
            else
            {
                radiusDifference = Math.Abs(lane1.DistanceFromCenter) + Math.Abs(lane2.DistanceFromCenter);
                tempLength = Math.Sqrt(Math.Pow(radiusDifference, 2) + Math.Pow(this.Length, 2));
            }

            return tempLength;
        }

        internal UniLane GetUniLane(int laneindex)
        {
            UniLane templane = new UniLane();

            foreach (UniLane lane in this.Lanes)
            {
                if (lane.Index == laneindex)
                {
                    templane = lane;
                    break;
                }
            }

            return templane;
        }

        public bool Equals(UniTrackPiece _piece)
        {
            if (this.Index == _piece.Index)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class UniLane
    {
        public int Index;
        public double Length;
        public int DistanceFromCenter;
        public int LaneRadius;

        public UniLane()
        {
            this.Index = 0;
            this.Length = 0.0;
            this.DistanceFromCenter = 0;
            this.LaneRadius = 0;
        }

        public UniLane(int _index, double _len, int _dist, int _lanerad)
        {
            this.Index = _index;
            this.Length = _len;
            this.DistanceFromCenter = _dist;
            this.LaneRadius = _lanerad;
        }

        public void setLength(double _newlen){
            this.Length = _newlen;
        }
    }
}
