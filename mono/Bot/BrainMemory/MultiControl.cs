﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamBarrelRollers.Data;
using TeamBarrelRollers.Net;
using TeamBarrelRollers.BrainMemory.Switcher;
using TeamBarrelRollers.BrainMemory.CarController;
using TeamBarrelRollers.BrainMemory.Analyst;
using TeamBarrelRollers.BrainMemory.Properties;
using TeamBarrelRollers.Utilities;

namespace TeamBarrelRollers.BrainMemory
{
    public class MultiControl
    {
        private StreamWriter writer;

        public TrackControl Tcontrol;
        public SwitchControl SControl;
        public CarControl CControl;
        public Analyzer Analyst;
        public List<BotCar> Cars;

        public BotCar OurBot;
        private string ourColor;
        private string ourName;

        public int SwitchingToLane = -1;
        public int SwitchingAt = 0;
        public bool sentswitch = false;

        public double Throttle { get; private set; }

        public MultiControl()
        {
            this.Cars = new List<BotCar>();
            this.Tcontrol = new TrackControl();
            this.SControl = new SwitchControl(Tcontrol);
            Consoler.StatsEnabled = true;
        }

        public void New() 
        {
            this.Cars = new List<BotCar>();
            this.Tcontrol = new TrackControl();
            this.SControl = new SwitchControl(Tcontrol);
        }

        public void Init(Race _race)
        {
            Consoler.Init(this);
            this.Tcontrol.Init(_race);
            this.SControl.Init(_race);

            foreach (Car car in _race.Cars)
            {
                BotCar tempCar = new BotCar(car.Id.Name, car.Id.Color);
                tempCar.Init(_race, Tcontrol);
                this.Cars.Add(tempCar);
            }

            foreach (BotCar bot in this.Cars)
            {
                if (bot.Color == this.ourColor)
                {
                    OurBot = bot;
                    break;
                }
            }

            SControl.SetOurBot(OurBot);
            SControl.SetCars(this.Cars);
            this.CControl = new CarControl(OurBot, Tcontrol, SControl);
            this.CControl.Init();

            this.Analyst = new Analyzer(this);
        }

        public void Update(CarPositions _pos)
        {
            
                foreach (CarPosition _posi in _pos.Positions)
                {
                    if (_posi.ID.Color == OurBot.Color && _posi.PiecePosition.InPieceDistance != null)
                    {
                        OurBot.Update(_pos, this.Tcontrol);
                        break;
                    }
                }
            
            this.SControl.Update(_pos);
            this.CControl.Update();

            this.Analyst.Update();
            Consoler.Update();
        }

        public void Crash(CrashWrapper _crash)
        {
            this.Analyst.Crash(_crash);
        }

        public SendMsg UpdateReply()
        {
            bool MsgFound = false;
            SendMsg tempMsg = new Ping();

            //Turboooooh!
            if (OurBot.TurboStatus == TurboState.Init)
            {
                tempMsg = new TurboMsg(OurBot.TurboTaunt);
                OurBot.TurboStatus = TurboState.On;
                MsgFound = true;
            }

            //Switch?
            if (!MsgFound)
            {
                if (SwitchingToLane == -1)
                {
                    this.SwitchingAt = SControl.getSwitchIndex(OurBot);
                    if (SwitchingAt != -1)
                    {
                        this.SwitchingToLane = Tcontrol.Track[SwitchingAt].LaneOut;
                    }
                }
                else
                {
                    if (OurBot.OnPiece.Index == SwitchingAt - 1)
                    {
                        string Message = SwitcherSolver.SolveSwitch(OurBot.LaneOut, SwitchingToLane);

                        if (Message != "")
                        {
                            tempMsg = new SwitchMsg(Message);
                            MsgFound = true;
                            sentswitch = true;
                        }
                    }
                }
                if (OurBot.OnPiece.Index == SwitchingAt)
                {
                    this.SwitchingToLane = -1;
                    sentswitch = false;
                }
            }

            //Throttle!
            if (!MsgFound)
            {
                double tempThrottle = 0.0;
                if (OurBot.CarThrottle >= 0)
                {
                    tempThrottle = OurBot.CarThrottle;
                }

                tempMsg = new Throttle(tempThrottle);
                Analyst.currentThrottle = tempThrottle;
                OurBot.LastsentThrottle = tempThrottle;
                MsgFound = true;

            }

            //Ping
            if (!MsgFound)
            {
                tempMsg = new Ping();
                MsgFound = true;
            }

            return tempMsg;
        }

        public void Start(YourBot _bot)
        {
            this.ourName = _bot.Name;
            this.ourColor = _bot.Color;
        }

        public void TurboGet(TurboWrapper turbo)
        {
            OurBot.GetTurbo(turbo);
        }

        public void SetWriter(ref StreamWriter _writer)
        {
            this.writer = _writer;
        }

    }

    #region Enums

    public enum Throttlestatus
    {
        Turn,
        TurnIncoming,
        Straight
    }

    public enum PieceType
    {
        Straight,
        StraightSwitcher,
        Turn,
        TurnSwitcher
    }

    #endregion
}
