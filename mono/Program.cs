using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using TeamBarrelRollers.Net;
using TeamBarrelRollers.Data;

namespace TeamBarrelRollers {
	public class Program {
		public static void Main(string[] args) {
		    string host = args[0];
	        int port = int.Parse(args[1]);
	        string botName = args[2];
	        string botKey = args[3];

			string track = "";
			int cars = 1;
			bool hoster = false;

			if(args.Length > 5)
			{
				track = args [4];
				cars = int.Parse (args [5]);
				if(args.Length > 6)
				{
					if (args [6] == "host")
						hoster = true;
				}
			}

			//while (true) 
			{

				try {
					using (TcpClient client = new TcpClient (host, port)) {
						NetworkStream stream = client.GetStream ();
						StreamReader reader = new StreamReader (stream);
						StreamWriter writer = new StreamWriter (stream);
						writer.AutoFlush = true;

						if(track != "")
						{
							new Bot (reader, writer, new BotId (botName, botKey), track, "nakkiperunat", cars, hoster);
						} 
						else 
						{
							new Bot (reader, writer, new Join (botName, botKey));
						}

					}
				} catch(Exception e) {
					Console.WriteLine (e);
					throw;
				}
			}
		}
	}
}
